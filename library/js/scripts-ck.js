/*
 * Bones Scripts File
 * Author: Eddie Machado
 *
 * This file should contain any js scripts you want to add to the site.
 * Instead of calling it in the header or throwing it inside wp_head()
 * this file will be called automatically in the footer so as not to
 * slow the page load.
 *
 * There are a lot of example functions and tools in here. If you don't
 * need any of it, just remove it. They are meant to be helpers and are
 * not required. It's your world baby, you can do whatever you want.
*//*
 * Get Viewport Dimensions
 * returns object with viewport dimensions to match css in width and height properties
 * ( source: http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript )
*/function updateViewportDimensions(){var e=window,t=document,n=t.documentElement,r=t.getElementsByTagName("body")[0],i=e.innerWidth||n.clientWidth||r.clientWidth,s=e.innerHeight||n.clientHeight||r.clientHeight;return{width:i,height:s}}var viewport=updateViewportDimensions(),waitForFinalEvent=function(){var e={};return function(t,n,r){r||(r="Don't call this twice without a uniqueId");e[r]&&clearTimeout(e[r]);e[r]=setTimeout(t,n)}}(),timeToWaitForLast=100;$(document).ready(function(){$(".float-label").label_better({easing:"bounce"});$("input.q3").on("input",function(e){if($(this).data("lastval")!=$(this).val()){$(this).data("lastval",$(this).val());if($(this).val()==""){$("input.q4").removeClass("qvisible");$("input.q4").addClass("qinvisible")}else{$("input.q4").removeClass("qinvisible");$("input.q4").addClass("qvisible")}}});$("input.q4").on("input",function(e){if($(this).data("lastval")!=$(this).val()){$(this).data("lastval",$(this).val());if($(this).val()==""){$("input.q5").removeClass("qvisible");$("input.q5").addClass("qinvisible")}else{$("input.q5").removeClass("qinvisible");$("input.q5").addClass("qvisible")}}});var e=160;$(".question").keyup(function(){var t=$(this).val().length,t=e-t;$("#chars").text(t)});var t=[3,7,15,30];$("#slider").slider({range:"min",value:0,min:0,max:t.length-1,step:1,slide:function(e,n){$("#amount").text(t[n.value])}});$("#create-btn").click(function(){$("#create-container").fadeIn(500);$(".header-action").addClass("active");$(".overlay").fadeIn(500);$("#nav-icon").css("display","none");$(".logo").css("display","none")});$(".create-close").click(function(){$("#create-container").fadeOut(500);$(".header-action").removeClass("active");$(".overlay").fadeOut(500);$("#nav-icon").css("display","inline-block");$(".logo").css("display","inline-block")});$("#nav-icon").click(function(){$(this).toggleClass("open");if($(this).hasClass("open")){$("#slide-menu").animate({top:"0"});$(".overlay").fadeIn(500)}else{$("#slide-menu").animate({top:"-400px"});$(".overlay").fadeOut(500)}})});